import cv2
import requests

# Путь к файлу
file_path = "tests/fixtures/fixtures/file_19.jpg"

# Открытие изображения с помощью OpenCV и преобразование в массив байтов
img = cv2.imread(file_path)
ret, img_bytes = cv2.imencode(".jpg", img)
img_bytes = img_bytes.tobytes()

# Отправка запроса на сервер
url = "http://0.0.0.0:8080/predict"
response = requests.post(url, data=img_bytes)

# Вывод ответа от сервера
print(response.text)
